<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DadosEstatisticos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dados_estatisticos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pontos')->nullable();
            $table->integer('faixa_etaria')->nullable();
            $table->integer('nivel_ensino')->nullable();
            $table->integer('cursou_calculo')->nullable();
            $table->integer('aprendeu')->nullable();
            $table->integer('jogar_vr')->nullable();
            $table->string('feedback')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}

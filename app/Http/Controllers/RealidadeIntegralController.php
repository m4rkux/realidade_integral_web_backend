<?php

namespace App\Http\Controllers;

use App\DadosEstatistico;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class RealidadeIntegralController extends Controller
{
    public function salvaDados (Request $request) 
    {
    	DadosEstatistico::create($request->all());
    }

    public function listDados (Request $request) 
    {
    	return DadosEstatistico::all();
    }
}

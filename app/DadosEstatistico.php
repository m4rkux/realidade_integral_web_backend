<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DadosEstatistico extends Model
{
    protected $fillable = ['pontos', 'faixa_etaria', 'nivel_ensino', 'cursou_calculo', 'aprendeu', 'jogar_vr', 'feedback'];
}
